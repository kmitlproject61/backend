import express from 'express'
import appRoutes from './route/route'
import bodyParser from 'body-parser'

const app = express()
const port = 8000
app.use(function (req, res, next) {
  res.setHeader('Access-Control-Allow-Origin', '*')
  res.setHeader('Access-Control-Allow-Methods', 'GET, POST')
  res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type')
  res.setHeader('Access-Control-Allow-Credentials', true)
  next()
})
// app.use(cors)
app.use(bodyParser.json())

app.use('/api/v1', appRoutes)
app.get('*', function (req, res) {
  res.status(404).send('404 NOT FOUND')
})

app.listen(port, () => console.log(`ABCast listening on port ${port}!`))
