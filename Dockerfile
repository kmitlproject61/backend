FROM node:9.10

WORKDIR /app

COPY . .

RUN yarn

EXPOSE 8000

RUN npm i -g nodemon

CMD ["yarn", "start"]
