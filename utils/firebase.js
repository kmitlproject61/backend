import firebase from 'firebase'

var config = {
  apiKey: 'AIzaSyAVNiOx9HIHtJsBQGKtVuK5KTo49B6xMtc',
  authDomain: 'abcast-backend.firebaseapp.com',
  databaseURL: 'https://abcast-backend.firebaseio.com',
  projectId: 'abcast-backend',
  storageBucket: 'abcast-backend.appspot.com',
  messagingSenderId: '1040649110909'
}
firebase.initializeApp(config)

export default firebase
