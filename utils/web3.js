import firebase from '../utils/firebase'
import Web3 from 'web3'

const db = firebase.firestore()
export const coinbase = '0x9a8bB255D578F1C1475473632878E954F71992d4'
const coinbasePrivateKey = '0x7e5384bd03061e7ad7445d3e95ace07ebad41e9975d96df46e86edbd2f470811'
const web3 = new Web3('http://rpc.z3n.pw')

// Create wallet for coinbase
web3.eth.accounts.wallet.clear()
web3.eth.accounts.wallet.create(1)
web3.eth.accounts.wallet.add(coinbasePrivateKey)

export function contractUser () {
  return db.collection('blockchains').doc('address').get()
    .then(resAddr => {
      // Get abi
      return db.collection('blockchains').doc('abi').get()
        .then(resABI => {
          return new web3.eth.Contract(resABI.data().abi, resAddr.data().address)
        })
        .catch(() => {
          console.log('get_firebase_error')
        })
    })
    .catch(() => {
      console.log('get_firebase_error')
    })
}

export default web3
