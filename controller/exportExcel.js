import fetch from 'node-fetch'
import xlsx from 'xlsx'
import fs from 'fs'

function importExcel (req, res) {
  if (!req.headers.authorization ||
      req.headers.authorization.split(' ').length < 2 ||
      !req.body.data) {
    res.status(400).json({ success: false, message: 'INVALID_REQUESTED' })
  } else {
    const { data } = req.body
    const idToken = req.headers.authorization.split(' ')[1]
    fetch(`https://oauth2.googleapis.com/tokeninfo?id_token=${idToken}`)
      .then(response => response.json())
      .then(gmail => {
        if (gmail.error) {
          res.status(400).json({ success: false, error: 'INVALID_TOKEN' })
        } else {
          if (gmail.hd !== 'kmitl.ac.th') {
            res.status(401).json({ success: false, error: 'EMAIL_MUST_BE_KMITL' })
          } else if (JSON.parse(gmail.email_verified) !== true) {
            res.status(401).json({ success: false, error: 'EMAIL_HAS_NOT_VERIFIED' })
          } else {
            // if (gmail.email.match('[0-9]{8}@kmitl.ac.th')) {
            if (false) {
              res.status(401).json({ success: false, error: 'AVAILABLE_ONLY_TEACHER' })
            } else {
              const path = 'exports/excels/'
              const fileName = `excel-${Date.now()}.xlsx`

              // Create workbook
              const ws = xlsx.utils.json_to_sheet(data)
              const wb = xlsx.utils.book_new()
              xlsx.utils.book_append_sheet(wb, ws, 'Sheet 1')
              const wbout = xlsx.write(wb, { type: 'binary', bookType: 'xlsx' })

              fs.writeFile(`${path}${fileName}`, wbout, 'ascii', (err) => {
                if (err) {
                  console.log(err)
                  res.status(400).json({ success: false, error: 'WRITE_FILE_ERROR' })
                }
                console.log('Successfully Written to File.')
                res.status(200).json({ success: true, file: `${path}${fileName}` })
              })
            }
          }
        }
      })
      .catch(e => {
        console.log('e: ', e)
        res.status(401).json({ success: false, error: 'LOGIN_FAILED' })
      })
  }
}

export default importExcel
