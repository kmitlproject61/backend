import fetch from 'node-fetch'
import web3, { coinbase, contractUser } from '../utils/web3'

function register (req, res) {
  if (!req.headers.authorization ||
        req.headers.authorization.split(' ').length < 2 ||
        !req.body.address ||
        !req.body.btAddr) {
    res.status(400).json({ success: false, message: 'INVALID_REQUESTED' })
  } else {
    const { address, btAddr } = req.body
    const idToken = req.headers.authorization.split(' ')[1]
    let role = 0
    let studentId
    fetch(`https://oauth2.googleapis.com/tokeninfo?id_token=${idToken}`)
      .then(response => response.json())
      .then(gmail => {
        if (gmail.error) {
          res.status(400).json({ success: false, error: 'INVALID_TOKEN' })
        } else {
          if (gmail.hd !== 'kmitl.ac.th') {
            res.status(401).json({ success: false, error: 'EMAIL_MUST_BE_KMITL' })
          } else if (JSON.parse(gmail.email_verified) !== true) {
            res.status(401).json({ success: false, error: 'EMAIL_HAS_NOT_VERIFIED' })
          } else {
            if (gmail.email.match('^[a-zA-Z][a-zA-Z0-9.]*@kmitl.ac.th')) {
              role = 1 // Teacher
              studentId = '00000000' // default by blockchain
            } else if (gmail.email.match('[0-9]{8}@kmitl.ac.th')) {
              role = 0 // Student
              studentId = gmail.email.substr(0, gmail.email.indexOf('@'))
            } else {
              res.status(401).json({ success: false, error: 'INVALID_KMITL_EMAIL' })
            }
            createUser(res, studentId, gmail, btAddr, role, address)
          }
        }
      })
      .catch(e => {
        console.log('e: ', e)
        res.status(400).json({ success: false, error: 'CREATE_ACCOUNT_FAILED' })
      })
  }
}

function createUser (res, studentId, gmail, btAddr, role, address) {
  contractUser().then(user => {
    user.methods.createUser(
      studentId,
      gmail.name,
      gmail.email,
      gmail.picture,
      web3.utils.stringToHex(btAddr),
      role,
      address
    ).send({
      from: coinbase,
      gas: 3000000
    }).then((result) => {
      if (Object.getOwnPropertyNames(result.events).length === 0) {
        res.status(401).json({ success: false, error: 'CREATE_USER_FAILED' })
      } else {
        res.status(201).json({ success: true })
      }
    }).catch((e) => {
      console.log('Create user failed')
      res.status(401).json({ success: false, error: 'CREATE_USER_FAILED' })
    })
  }).catch(() => {
    res.status(401).json({ success: false, error: 'USER_CONTRACT_IS_INVALID' })
  })
}

export default register
