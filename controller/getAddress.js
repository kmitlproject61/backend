import firebase from '../utils/firebase'

const db = firebase.firestore()

function getAddress (req, res) {
  // Get address
  db.collection('blockchains').doc('address').get()
    .then(addr => {
      // Get abi
      db.collection('blockchains').doc('abi').get()
        .then(abi => {
          const obj = Object.assign(addr.data(), abi.data())
          res.status(200).json(Object.assign({ result: true }, obj))
        })
        .catch(() => {
          res.status(401).json({ result: false, message: 'get_firebase_error' })
        })
    })
    .catch(() => {
      res.status(401).json({ result: false, message: 'get_firebase_error' })
    })
}

export default getAddress
