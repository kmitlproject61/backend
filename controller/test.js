import fetch from 'node-fetch'

function test (req, res) {
  fetch(`https://oauth2.googleapis.com/tokeninfo?id_token=${req.params.idToken}`)
    .then(response => response.json())
    .then(gmail => {
      if (gmail.error) {
        console.log('gmail.error: ', gmail.error)
        res.status(401).json({ result: false, error: gmail.error })
      }
      res.status(200).json({ result: true, gmail })
    })
    .catch(e => console.log('catch: ', e))
}

export default test
