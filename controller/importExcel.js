import fetch from 'node-fetch'
import multer from 'multer'
import xlsx from 'xlsx'

const storage = multer.diskStorage({
  destination: function (req, file, callback) {
    if (file.mimetype !== 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet') {
      callback('file is not support')
    }
    callback(null, './uploads/excels')
  },
  filename: function (req, file, callback) {
    if (file.mimetype !== 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet') {
      callback('file is not support')
    }
    callback(null, file.fieldname + '-' + Date.now() + '.xlsx')
  }
})
const upload = multer({ storage: storage }).single('excel')

function importExcel (req, res) {
  if (!req.headers.authorization ||
      req.headers.authorization.split(' ').length < 2) {
    res.status(400).json({ success: false, message: 'INVALID_REQUESTED' })
  } else {
    const idToken = req.headers.authorization.split(' ')[1]
    fetch(`https://oauth2.googleapis.com/tokeninfo?id_token=${idToken}`)
      .then(response => response.json())
      .then(gmail => {
        if (gmail.error) {
          res.status(400).json({ success: false, error: 'INVALID_TOKEN' })
        } else {
          if (gmail.hd !== 'kmitl.ac.th') {
            res.status(401).json({ success: false, error: 'EMAIL_MUST_BE_KMITL' })
          } else if (JSON.parse(gmail.email_verified) !== true) {
            res.status(401).json({ success: false, error: 'EMAIL_HAS_NOT_VERIFIED' })
          } else {
            // if (gmail.email.match('[0-9]{8}@kmitl.ac.th')) {
            if (false) {
              res.status(401).json({ success: false, error: 'AVAILABLE_ONLY_TEACHER' })
            } else {
              upload(req, res, function (err) {
                if (err) {
                  if (err === 'file is not support') {
                    return res.status(401).json({ success: false, error: 'FILE_IS_NOT_SUPPORT' })
                  }
                  return res.status(400).json({ success: false, error: 'CANNOT_READ_EXCEL_FILE' })
                }
                const workbook = xlsx.readFile(req.file.path)
                const sheets = Object.keys(workbook.Sheets)
                const datas = workbook.Sheets[sheets[0]] // first sheet - any name
                const keys = Object.keys(datas)
                const cells = keys.slice(1, keys.length - 1)
                const studentsId = []
                cells.map((cell, i) => {
                  // Push only column A2+
                  if (parseInt(cell.substring(1, cell.length)) !== 1 && cell.substring(0, 1) === 'A') {
                    studentsId.push(datas[cell].v)
                  }
                })
                res.status(200).json({ success: true, data: studentsId })
              })
            }
          }
        }
      })
      .catch(e => {
        console.log('e: ', e)
        res.status(401).json({ success: false, error: 'LOGIN_FAILED' })
      })
  }
}

export default importExcel
