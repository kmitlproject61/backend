import fetch from 'node-fetch'
import web3, { contractUser } from '../utils/web3'

function login (req, res) {
  if (!req.headers.authorization ||
      req.headers.authorization.split(' ').length < 2 ||
      !req.body.address) {
    res.status(400).json({ success: false, message: 'INVALID_REQUESTED' })
  } else {
    const { address } = req.body
    const idToken = req.headers.authorization.split(' ')[1]
    fetch(`https://oauth2.googleapis.com/tokeninfo?id_token=${idToken}`)
      .then(response => response.json())
      .then(gmail => {
        if (gmail.error) {
          res.status(400).json({ success: false, error: 'INVALID_TOKEN' })
        } else {
          if (gmail.hd !== 'kmitl.ac.th') {
            res.status(401).json({ success: false, error: 'EMAIL_MUST_BE_KMITL' })
          } else if (JSON.parse(gmail.email_verified) !== true) {
            res.status(401).json({ success: false, error: 'EMAIL_HAS_NOT_VERIFIED' })
          } else {
            // Check account address exist
            contractUser().then(user => {
              user.methods.getUser(address).call({
                from: address
              }).then((user) => {
                web3.eth.getBalance(address).then((balance) => {
                  if (user[0] === '') {
                    res.status(401).json({ success: false, error: 'INVALID_ADDRESS' })
                  } else if (balance <= 0) {
                    res.status(401).json({ success: false, error: 'INVALID_ADDRESS' })
                  } else {
                    res.status(200).json({ success: true })
                  }
                }).catch(() => {
                  res.status(401).json({ success: false, error: 'NO_ADDRESS_EXIST' })
                })
              }).catch(() => {
                res.status(401).json({ success: false, error: 'NO_ADDRESS_EXIST' })
              })
            })
          }
        }
      })
      .catch(e => {
        console.log('e: ', e)
        res.status(401).json({ success: false, error: 'LOGIN_FAILED' })
      })
  }
}

export default login
