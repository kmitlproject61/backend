
function adminLogin (req, res) {
  if (!req.body.email) {
    res.status(400).json({ success: false, message: 'INVALID_REQUESTED' })
  } else {
    const { email, hd } = req.body
    const studentId = email.substr(0, email.indexOf('@'))
    if (hd === 'kmitl.ac.th') {
      if (studentId === '58010418' || studentId === '58010176') {
        res.status(200).json({ success: true })
      } else {
        res.status(401).json({ success: false, message: 'permission_denied' })
      }
    } else {
      res.status(400).json({ success: false, message: 'email_must_be_kmitl' })
    }
  }
}

export default adminLogin
