import express from 'express'
import register from '../controller/register'
import login from '../controller/login'
import importExcel from '../controller/importExcel'
import exportExcel from '../controller/exportExcel'
import adminLogin from '../controller/adminLogin'
import getAddress from '../controller/getAddress'
import test from '../controller/test'
const router = express.Router()
router.use('/exports', express.static('exports'))

// Application
router.post('/register', register)
router.post('/login', login)
router.post('/import-excel', importExcel)
router.post('/export-excel', exportExcel)

// Dashboard Admin
router.post('/admin/login', adminLogin)
router.get('/admin/address', getAddress)

// Testing
router.get('/test-api/:idToken', test)
router.get('/test', (req, res) => res.send('API is working'))

export default router
